想写一个输入法，用什么语言写还没想好，可能是 lua 也可能是 fennel，但更可能是二者混合，暂时以 (neo)vim 为平台写一个插件。首先支持四码定长的形码，拼音或其他输入方案暂置。

**WARNING 警告**：插件仍在开发阶段，功能暂未完成，已完成功能不健全不稳定，请勿下载！！！请勿下载！！！请勿下载！！！

**WARNING 警告**：请勿将本插件直接放到 `~/.local/share/nvim/plugin/` 目录下，插件暂未保证程序文件的启动顺序，请参照如下配置进行设置

```lua
-- in ~/.config/nvim/init.lua
vim.opt.rtp:append("/path/to/plugin") -- for example "~/im.nvim"

require('im_nvim').setup({
  db_path = '/path/to/database.db', -- default as the codes.db in im.nvim/lua/
  sqlite_path = '/path/to/sqlite',  -- default as 'sqlite3'
})
```


---
--- 1. 码表加载模块
---

local base_dict = {}

function base_dict:handleCandidate(str, func, opts)
  -- 根据使用的码表 im 读取不同的文件
  local limit = opts and opts.limit or 25
  local offset = opts and opts.offset or 0
  local fuzzy = opts and opts.fuzzy or true

  local match_id = vim.fn.match(self.db, '^\\V' .. str .. (fuzzy and '' or ' ')) + 1
  if match_id == 0 then
    print("Query failed.")
    return nil
  end

  match_id = match_id + offset
  for i = match_id, match_id + limit do
    local v = vim.split(self.db[i], ' ')[2]
    func(str, v)
  end
end

local function setup_dict(dict_path)
  local dict = {}
  local fin = io.open(dict_path, 'r')
  assert(fin,  "Lookup table open failed.")

  for line in fin:lines() do
    table.insert(dict, line)
  end

  fin:close()
  return setmetatable({db = dict}, {__index = base_dict})
end

---
--- 2. 一个简单有限状态机的实现
---

local empty_mode = {}
local chinese_mode = {}
local tmp_en_mode = {}

---
--- General Function
---

-- TODO: characters filter
-- TODO: replace loadConfig with vim.tbl_deep_extend

-- For output the original input chars
local function normal(mode, env, input, output, params)
  output.output = nil
  output.confirm = true
  env:reset()
  return mode
end

-- Return a callback function of the db:handleCandidate
local function updateCandidate(env, output)
  local index = 0
  return function(code, char)
    index = index + 1
    env.candidate[index] = char
    env.candidate_size = index

    local displayId = index - env.page_offset
    if displayId <= env.page_size and displayId > 0 then
      output.display[displayId] = char
    end
  end
end

---
--- Empty Mode
---

empty_mode.config = {
  ['0'] = {'number'}, ['1'] = {'number'}, ['2'] = {'number'}, ['3'] = {'number'}, ['4'] = {'number'},
  ['5'] = {'number'}, ['6'] = {'number'}, ['7'] = {'number'}, ['8'] = {'number'}, ['9'] = {'number'},
  a = {'letter'}, b = {'letter'}, c = {'letter'}, d = {'letter'}, e = {'letter'}, f = {'letter'}, g = {'letter'},
  h = {'letter'}, i = {'letter'}, j = {'letter'}, k = {'letter'}, l = {'letter'}, m = {'letter'}, n = {'letter'},
  o = {'letter'}, p = {'letter'}, q = {'letter'}, r = {'letter'}, s = {'letter'}, t = {'letter'},
  u = {'letter'}, v = {'letter'}, w = {'letter'}, x = {'letter'}, y = {'letter'}, z = {'letter'},
  [','] = {'punc'}, ['.'] = {'punc'}, ['/'] = {'toTmpEn'}, [';'] = {'punc'}, ['\''] = {'punc'},
  ['<'] = {'punc'}, ['>'] = {'punc'}, ['?'] = {'punc'}, [':'] = {'punc'}, ['\"'] = {'punc'},
  ['['] = {'punc'}, [']'] = {'punc'}, ['\\'] = {'punc'}, ['-'] = {'punc'}, ['='] = {'punc'},
  ['{'] = {'punc'}, ['}'] = {'punc'}, ['|'] = {'punc'}, ['_'] = {'punc'}, ['+'] = {'punc'},
  ['!'] = {'punc'}, ['@'] = {'punc'}, ['#'] = {'punc'}, ['$'] = {'punc'}, ['%'] = {'punc'},
  ['^'] = {'punc'}, ['&'] = {'punc'}, ['*'] = {'punc'}, ['('] = {'punc'}, [')'] = {'punc'},
  ['`'] = {'punc'}, ['~'] = {'punc'},
}

function empty_mode:letter(env, input, output)
  env.number_prefix = false
  if #env.input >= 4 then
    self:select(env, input, output)
  end

  env.page_offset = 0
  env.input = env.input .. input

  output.input = env.input
  env.db:handleCandidate(env.input, updateCandidate(env, output))
  return chinese_mode
end

function empty_mode:number(env, input, output)
  env.number_prefix = true
  output.confirm = true
  output.output = nil
  return empty_mode
end

function empty_mode:punc(env, input, output)
  local punc = env.config.punc
  output.confirm = true

  -- TODO: add an origin-output puncs table
  if env.number_prefix and (input == '.' or input == ':') then
    env.number_prefix = false
    output.output = nil
    return empty_mode
  end

  env.number_prefix = false
  if type(punc[input]) == 'table' then
    local state = env.punc_state[input] or 1
    env.punc_state[input] = state % #punc[input] + 1
    output.output = output.output .. punc[input][state]
  else
    output.output = output.output .. punc[input]
  end
  return empty_mode
end

function empty_mode:toTmpEn(env, input, output)
  output.confirm = true
  env.number_prefix = false
  return tmp_en_mode
end

---
--- Chinese Mode
---

chinese_mode.config = vim.tbl_deep_extend("force", empty_mode.config, {
  ['1'] = {'select', params = {id = 1}}, ['2'] = {'select', params = {id = 2}},
  ['3'] = {'select', params = {id = 3}}, ['4'] = {'select', params = {id = 4}},
  ['5'] = {'select', params = {id = 5}}, ['6'] = {'select', params = {id = 6}},
  ['7'] = {'select', params = {id = 7}}, ['8'] = {'select', params = {id = 8}},
  ['9'] = {'select', params = {id = 9}},
  [' '] = {'select', params = {id = 1}}, [';'] = {'select', params = {id = 2}},
  ['\''] = {'select', params = {id = 3}}, ['\t'] = {'select', params = {id = 4}},
  ['['] = {'page', params = {isUp = false}}, [']'] = {'page', params = {isUp = true}},
  -- [','] = {'page', params = {isUp = false}}, ['.'] = {'page', params = {isUp = true}},
  ['\n'] = {'enter'}, ['\027'] = {'clear'},
  ['\b'] = {'backspace'}, ['/'] = {'punc'},
  -- ['+'] = {'add'}, ['-'] = {'delete'},
  A = {'caps'}, B = {'caps'}, C = {'caps'}, D = {'caps'}, E = {'caps'}, F = {'caps'}, G = {'caps'},
  H = {'caps'}, I = {'caps'}, J = {'caps'}, K = {'caps'}, L = {'caps'}, M = {'caps'}, N = {'caps'},
  O = {'caps'}, P = {'caps'}, Q = {'caps'}, R = {'caps'}, S = {'caps'}, T = {'caps'},
  U = {'caps'}, V = {'caps'}, W = {'caps'}, X = {'caps'}, Y = {'caps'}, Z = {'caps'},
})

chinese_mode.letter = empty_mode.letter

function chinese_mode:caps(env, input, output)
  self:select(env, input, output)
  output.output = output.output .. ' ' .. input
  return empty_mode
end

function chinese_mode:punc(env, input, output)
  self:select(env, input, output)
  empty_mode:punc(env, input, output)
  return empty_mode
end

function chinese_mode:select(env, input, output, params)
  local index = env.page_offset + (params and params.id or 1)
  local selected_char = index <= env.candidate_size and env.candidate[index] or ''
  env:reset()
  output:reset()

  output.confirm = true
  output.output = selected_char
  return empty_mode
end

function chinese_mode:clear(env, input, output)
  output.confirm = true
  env:reset()
  return empty_mode
end

function chinese_mode:backspace(env, input, output)
  local len = #env.input
  if len ~= 0 then
    env.input = env.input:sub(1, -2)
  end

  if len == 1 then
    env:reset()
    output.confirm = true
    output.output = ''
    return empty_mode
  else
    env.db:handleCandidate(env.input, updateCandidate(env, output))
    output.input = env.input
    return chinese_mode
  end
end

function chinese_mode:enter(env, input, output)
  output.output = env.input
  output.confirm = true
  env:reset()
  return empty_mode
end

function chinese_mode:page(env, input, output, params)
  if params and params.isUp and env.candidate_size >= (env.page_offset + env.page_size + 1) or not params then
    env.page_offset = env.page_offset + env.page_size
  elseif params and not params.isUp and env.page_offset > 0 then
    env.page_offset = env.page_offset - env.page_size
  end

  local offset = env.page_offset
  output.input = env.input
  for i = 1, env.page_size do
    output.display[i] = (offset + i) <= env.candidate_size and env.candidate[offset + i] or nil
  end
  return chinese_mode
end

---
--- Temp Engligh Mode (Or Command Mode?)
---

tmp_en_mode.config = {
  ['/'] = {'punc'}, [' '] = {'punc'}, ['\n'] = {'toEmpty'},
  [':'] = {'toCmd'}, ['\027'] = {'toEmpty'}
}

tmp_en_mode.toCmd = normal

function tmp_en_mode:toEmpty(env, input, output)
  output.confirm = true
  return empty_mode
end

function tmp_en_mode:punc(env, input, output)
  empty_mode:punc(env, input, output)
  return empty_mode
end

---
--- Interface
---

local function im_generator(db, user_config)
  local fsm = empty_mode
  local env = {
    db = db,  -- db = { db = db(cdata), base_db functions}
    -- TODO: buffer and offset
    config = user_config,
    number_prefix = false,
    punc_state = {},

    input = '',
    page_offset = 0,
    page_size = 5,
    candidate_size = 0,
    candidate = {},

    reset = function(self)
      self.number_prefix = false
      self.input = ''
      self.page_offset = 0
      self.candidate_size = 0
      for k in pairs(self.candidate) do self.candidate[k] = nil end
    end,
  }
  -- @param output: nil for origin input and '' for empty output, when
  --                confirm is true the output is written on the screen.
  -- @param confirm: whether the output should be written.
  -- @param page_offset: work with display and candidate, which range of
  --                    candidate items will be displayed.
  --                    (moved to the env)
  -- @param candidate: all output items.
  --                   (moved to the env)
  -- @param display: items to be displayed, include items to be selected
  --                 and input chars.
  -- @param input: what codes user input. See usage below.
  --
  -- confirm = true and output = nil means the origin input will be written
  -- confirm = true and input = nil means the IMInserted should be
  --   triggered.
  local output = {
    input = nil,
    output = '',
    confirm = false,
    display = {},  -- include input item to display
    reset = function(self)
      self.input = nil
      self.output = ''
      self.confirm = false
      for k in pairs(self.display) do self.display[k] = nil end
    end,
  }

  return function(input)
    output:reset()
    local func = fsm.config[input] or {}
    local f = func[1] and fsm[func[1]] or normal
    fsm = f(fsm, env, input, output, func.params)
    return output
  end
end

---
--- 3. 插件主体
---

local db
local config = {
  db_path = vim.fn.stdpath('config')..'sime.txt',
  im_name = "yu",  -- table name
  punc = { -- punc should include as much as keys
    [' '] = ' ', ['\t'] = '\t', ['`'] = '`', ['~'] = '·',
    ['.'] = '。', [','] = '，', ['/'] = '/', [';'] = '；', ['\''] = {'‘', '’'},
    ['<'] = '《', ['>'] = '》', ['?'] = '？', [':'] = '：', ['\"'] = {'“', '”'},
    ['['] = '【', [']'] = '】', ['\\'] = '、', ['-'] = '-', ['='] = '=',
    ['{'] = '〖', ['}'] = '〗', ['|'] = '|', ['_'] = '——', ['+'] = '+',
    ['!'] = '！', ['@'] = '@', ['#'] = '#', ['$'] = '￥', ['%'] = '%',
    ['^'] = '……', ['&'] = '&', ['*'] = '*', ['('] = '（', [')'] = '）',
  },
}
local M = {}

local convert = {
  ['<ESC>'] = '\027', ['<CR>'] = '\n', ['<SPACE>'] = ' ', ['<TAB>'] = '\t', ['<BS>'] = '\b',
}

local popup_bar = {}
local popup_opts = {
  id = 1,
  virt_lines = {popup_bar},
  virt_lines_above = false,
  virt_text_pos = "overlay",
}

-- handle the output table, including displaying candidate bar
local function handler(im, input)
  local ch = convert[input] or input
  return function()
    local output = im(ch)

    for k, _ in pairs(popup_bar) do popup_bar[k] = nil end
    local i = 1
    popup_bar[i] = {string.format("%-4s", output.input or ''), "PmenuSel"}
    for _, v in ipairs(output.display) do
      i = i + 1
      popup_bar[i] = {' ' .. tostring(i - 1) .. ' ' .. v, "Pmenu"}
    end

    -- Get cursor position
    local current_row = vim.api.nvim_win_get_cursor(0)[1] - 1
    -- popup_opts.virt_lines_above = current_row ~= 0
    vim.api.nvim_buf_set_extmark(0, vim.b.ns_id, current_row, 0, popup_opts)

    if output.confirm then
      if not output.input then
        vim.cmd [[doautocmd User IMInserted]]
      else
        vim.cmd [[echohl Pmenu | echon "hi"]]
      end
      return output.output or input
    end
  end
end

---
--- Plugin Setup(TODO: lazy load)
---

local function bufSetup(user_config)
  vim.b.ns_id = vim.api.nvim_create_namespace("im" .. tonumber(vim.api.nvim_get_current_buf()))
  vim.b.keymap_name = user_config.im_name
  vim.b.im = im_generator(db, user_config)

  -- keymap set: all puncs and letters
  for i = 33, 126 do
    vim.keymap.set('l', string.char(i), handler(vim.b.im, string.char(i)),
      { expr = true, noremap = true, buffer = false, silent = true, desc = "Input Method Keymap" })
  end
  for k, _ in pairs(convert) do
    vim.keymap.set('l', k, handler(vim.b.im, k),
      { expr = true, noremap = true, buffer = false, silent = true })
  end
end

function M.setup(user_config)
  config = vim.tbl_deep_extend('force', config, user_config)
  db = setup_dict(config.db_path)
  bufSetup(config)

  vim.keymap.set({'i', 'c'}, '<C-S>', '<C-^>',
    { noremap = true, buffer = false, silent = true })
  vim.keymap.set('n', '<C-S>', 'a<C-^><ESC>',
    { noremap = true, buffer = false, silent = true })
end

---
--- AutoCommand
---

vim.api.nvim_create_autocmd({'User'}, {
  pattern = 'IMInserted',
  callback = function()
    vim.api.nvim_buf_clear_namespace(0, vim.b.ns_id, 0, -1)
  end,
})
vim.api.nvim_create_autocmd({'BufWinEnter'}, {
  pattern = "*",
  callback = function()
    bufSetup(config)
  end,
})

return M
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$:

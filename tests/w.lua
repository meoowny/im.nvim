local buf = vim.api.nvim_create_buf(false, true)
local t = {'test', 'hope'}
vim.api.nvim_buf_set_lines(buf, 0, -1, true, t)
local opts = {
    --relative = 'cursor', anchor = 'NW',
    relative = 'editor',
    width = 13, height = 3,
    row = 2, col = 0,
    style = 'minimal',
}
local open_win = function()
    opts.height = #t + 1
    vim.api.nvim_buf_set_lines(buf, 0, -1, true, t)
    vim.b.win = vim.api.nvim_open_win(buf, false, opts)
end
local close_win = function()
    vim.api.nvim_win_close(vim.b.win, true)
end
local update_win = function()
    t = {'hhh'}
    vim.api.nvim_buf_set_lines(buf, 0, -1, true, t)
end
open_win() close_win()
vim.keymap.set('n', 'gy', open_win)
vim.keymap.set('n', 'gr', close_win)
vim.keymap.set('n', 'gh', update_win)

vim.b.win = vim.api.nvim_open_win(buf, false, opts)
vim.api.nvim_win_close(vim.b.win, true)


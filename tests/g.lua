local t = {{'test', 'IncSearch'}, {'hope'}}
local ns_id = vim.api.nvim_create_namespace("im")
local opts = {
    id = 1,
    virt_text = t,
    --virt_lines = {t, t},
    --virt_text_pos = "right_align",
    virt_text_pos = "overlay",
    --virt_lines_leftcol = true,
    ui_watched = true,
    --virt_text_win_col = 2,
}
local width = vim.api.nvim_win_get_width(0) - vim.o.numberwidth

local function display()
    local current_row, current_col = unpack(vim.api.nvim_win_get_cursor(0))
    --current_row = current_row - 1
    local too_long = current_col >= width
    if too_long then
        current_row = current_row - 1
        current_col = current_col - width
    else
        current_col = current_col % width
        current_col = (current_col + 8 < width) and current_col or width - 8
    end
    --current_col = (current_col + 8 < width) and current_col or width - 8
    --opts.virt_text_win_col = current_col
    vim.api.nvim_buf_set_extmark(0, ns_id, current_row, current_col, opts)
end

vim.api.nvim_create_autocmd({'InsertEnter'}, {
    pattern = '*',
    callback = function() display() end,
})
vim.api.nvim_create_autocmd({'InsertLeavePre'}, {
    pattern = '*',
    callback = function() vim.api.nvim_buf_clear_namespace(0, ns_id, 0, -1) end,
})

local t = {}
local mt = {
    __index = {
        a = 10,
        b = 20,
        add = function (a, b)
            return a + b
        end
    }
}

-- lua 的 class 实现：https://zhuanlan.zhihu.com/p/123971515
-- https://zhuanlan.zhihu.com/p/268643546
-- 基础方法讲解：https://zhuanlan.zhihu.com/p/161246590
-- 一个完整的 github 仓库中的讲解：https://github.com/wzhengsen/LuaOOP

setmetatable(t, mt)
print(string.format("%d + %d = %d", t.a, t.b, t.add(t.a, t.b)))

-- 声明一个 lua class
local function class(className, super)
    -- 构建类
    local clazz = { __cname = className, super = super }
    if super then
        -- 设置父类
        setmetatable(clazz, { __index = super })
    end
    -- new 方法创建类对象
    clazz.new = function (...)
        -- 构造一个对象
        local instance = {}
        -- 设置对象的元表为当前类，以使对象可调用当前类声明的方法
        setmetatable(instance, { __index = clazz })
        if clazz.ctor then
            clazz.ctor(instance, ...)
        end
        return instance
    end
    return clazz
end

local printf = function (str, ...)
    return print(string.format(str, ...))
end

-- 声明 classA
local classA = class("classA")
classA.static = 'Static A'
function classA:ctor(a, b)
    self.a = a or 0
    self.b = b or 0
end

function classA:print()
    printf("%s, a = %s, b = %d, static = %s", self.__cname, self.a, self.b, self.static)
end

function classA:getSum()
    return self.a + self.b
end

-- 声明 classB 并继承 classA
local classB = class("classB", classA)
function classB:ctor(...)
    self.super.ctor(self, ...)
end

-- overwrite
function classB:print()
    print('class B overwrite super print')
end

-- 声明 classC 并继承 classB
local classC = class("classC", classA)
classC.static = 'static c'

local obja1 = classA.new(10, 20)
local obja2 = classA.new(100, 200)
local objb1 = classB.new(1, 2)
local objc  = classC.new(3, 4)
obja1:print()
obja2:print()
objb1:print()
objc:print()
printf("3 + 4 = %s", objc:getSum())


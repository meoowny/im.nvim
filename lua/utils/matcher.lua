local base_dict = {}

function base_dict:open(name)
  -- To be compatible with sqlite version
  return name
end

local handleCandidate_default_opts = {
  limit = 25,
  offset = 0,
  fuzzy = true,
}
function base_dict:handleCandidate(im, str, func, opts)
  -- 根据使用的码表 im 读取不同的文件
  local limit = opts and opts.limit or handleCandidate_default_opts.limit
  local offset = opts and opts.offset or handleCandidate_default_opts.offset
  local fuzzy = opts and opts.fuzzy or handleCandidate_default_opts.fuzzy

  local match_id = vim.fn.match(self.db, '^\\V' .. str .. (fuzzy and '' or ' ')) + 1
  if match_id == 0 then
    print("Query failed.")
    do return nil end
  end

  match_id = match_id + offset
  for i = match_id, match_id + limit do
    local v = vim.split(self.db[i], ' ')[2]
    func(str, v)
  end
end

function base_dict:close()
  -- To be compatible with sqlite version
  return nil
end

local function setup(_, dict_path)
  local dict = {}
  local fin = io.open(dict_path, 'r')
  assert(fin,  "Lookup table open failed.")

  for line in fin:lines() do
    table.insert(dict, line)
  end

  fin:close()
  return setmetatable({db = dict}, {__index = base_dict})
end

return setup
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$ fdm=indent:

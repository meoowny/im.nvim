-- local fhandle = io.tmpfile()
-- local fhandle = io.popen()

---
--- Sqlite Database Interface
---

local base_db = {}
local sqlite

---
--- Base Functions
---

local CREATE = [[
CREATE TABLE IF NOT EXISTS %s(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  key TEXT NOT NULL,
  value TEXT NOT NULL,
  weight INTEGER DEFAULT 0
); ]]

-- key ASC, weight DESC or weight DESC, key ASC?
local QUERY = [[
  SELECT key, value
  FROM %s
  WHERE key LIKE '%s'
  ORDER BY key ASC, weight DESC
  LIMIT %d OFFSET %d; ]]

function base_db:open(name)
  self.db:execf(CREATE, name)
end

local handleCandidate_default_opts = {
  limit = 25,
  offset = 0,
  fuzzy = true,
}
function base_db:handleCandidate(im, str, func, opts)
  local limit = opts and opts.limit or handleCandidate_default_opts.limit
  local offset = opts and opts.offset or handleCandidate_default_opts.offset
  local fuzzy
  if opts and opts.fuzzy ~= nil then
    fuzzy = opts.fuzzy
  else
    fuzzy = handleCandidate_default_opts.fuzzy
  end
  str = fuzzy and (str .. '%') or str
  local code, stmt = self.db:prepare_v2(QUERY:format(im, str, limit, offset))
  assert(code == sqlite.const.OK, "Query failed: " .. self.db:errmsg())
  for _ in stmt:iter() do
    func(stmt:column_text(0), stmt:column_text(1))
  end
  stmt:finalize()
end

function base_db:close()
  self.db:close()
end

---
--- More Function
---

local TOTAL_QUERY = [[
  SELECT key, value, weight
  FROM %s
  ORDER BY key ASC, weight DESC; ]]

function base_db:convert(im_name, outfile, with_weight)
  local fout = io.open(outfile, "w")
  assert(fout, "fout open failed.")

  local code, stmt = self.db:prepare_v2(TOTAL_QUERY:format(im_name))
  assert(code == self.const.OK, "Query failed: " .. self.db:errmsg())

  if with_weight then
    for _ in stmt:iter() do
      fout:write(stmt:column_text(0) .. ' ' .. stmt:column_text(1) .. ' ' .. stmt:column_int(2) .. '\n')
    end
  else
    for _ in stmt:iter() do
      fout:write(stmt:column_text(0) .. ' ' .. stmt:column_text(1) .. '\n')
    end
  end

  stmt:finalize()
  fout:close()
end

local DELETE = [[
  DELETE FROM %s
  WHERE weight = 101 - %d AND key LIKE '%s'; ]]
local INCREASE_WEIGHT = [[
  UPDATE %s
  SET weight = weight + 1
  WHERE weight <= 101 - %d AND key LIKE '%s'; ]]

function base_db:remove(im, key, id)
  id = id or 1
  local code, stmt = self.db:prepare_v2(DELETE:format(im, id, key))
  assert(code == self.const.OK, "Remove failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()

  code, stmt = self.db:prepare_v2(INCREASE_WEIGHT:format(im, id, key))
  assert(code == self.const.OK, "Data update after remove failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()
end

local INSERT = [[
  INSERT INTO %s(key, value, weight)
  VALUES('%s', '%s', 101 - %d) ]]
local DECREASE_WEIGHT = [[
  UPDATE %s
  SET weight = weight - 1
  WHERE weight <= 101 - %d AND key LIKE '%s'; ]]
local QUERY_FOR_INSERT = [[
  SELECT *
  FROM %s
  WHERE key LIKE '%s' AND value LIKE '%s'; ]]

function base_db:insert(im, key, char, id)
  id = id or 1
  local code, stmt = self.db:prepare_v2(QUERY_FOR_INSERT:format(im, key, char))
  assert(code == self.const.OK, "Query for insert failed: " .. self.db:errmsg())
  local cnt = 0
  for _ in stmt:iter() do
    cnt = cnt + 1
  end
  if cnt > 0 then
    -- adjusting freq
    do return nil end
  end

  id = id or 1
  code, stmt = self.db:prepare_v2(DECREASE_WEIGHT:format(im, id, key))
  assert(code == self.const.OK, "Data update before insert failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()

  code, stmt = self.db:prepare_v2(INSERT:format(im, key, char, id))
  assert(code == self.const.OK, "Insert failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()
end

local EXCHANGE_WEIGHT1 = [[
  UPDATE %s
  SET weight = 101
  WHERE weight == 101 - %d AND key LIKE '%s'; ]]
local EXCHANGE_WEIGHT2 = [[
  UPDATE %s
  SET weight = 101 - %d
  WHERE weight == 101 - %d AND key LIKE '%s'; ]]
local EXCHANGE_WEIGHT3 = [[
  UPDATE %s
  SET weight = 101 - %d
  WHERE weight == 101 AND key LIKE '%s'; ]]

function base_db:exchange(im, key, id1, id2)
  assert(key and id1 and id2, "Missing parameters in exchange")
  -- TODO: check for id range
  if id1 ~= id2 then
    do return nil end
  end

  local code, stmt = self.db:prepare_v2(EXCHANGE_WEIGHT1:format(im, id1, key))
  assert(code == self.const.OK, "Data update failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()

  code, stmt = self.db:prepare_v2(EXCHANGE_WEIGHT2:format(im, id1, id2, key))
  assert(code == self.const.OK, "Data update failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()

  code, stmt = self.db:prepare_v2(EXCHANGE_WEIGHT3:format(im, id2, key))
  assert(code == self.const.OK, "Data update failed: " .. self.db:errmsg())
  stmt:step()
  stmt:reset()
end

local GET_FULL_KEY = [[
  SELECT key
  FROM %s
  WHERE value LIKE '%s' AND LENGTH(key) = 4; ]]

function base_db:getFullKey(im, char)
  local full_key = nil
  local code, stmt = self.db:prepare_v2(GET_FULL_KEY:format(im, char))
  assert(code == self.const.OK, "Key get failed: " .. self.db:errmsg())
  for _ in stmt:iter() do
    assert(not full_key, 'Multikeys for ' .. (full_key or '()'))
    full_key = stmt:column_text(0)
  end
  stmt:finalize()
  full_key = full_key or ''
  assert(type(full_key) == 'string', 'Full key got is not string: ' .. type(full_key))
  return full_key
end

---
--- Module Setup
---

local function setup(sqlite_path, db_path)
  sqlite = require 'utils/sqlite3'(sqlite_path)
  local code, db = sqlite.open(db_path)
  assert(code == sqlite.const.OK, "Error(with " .. sqlite_path .. " and " .. db_path .. "): " .. db:errmsg())
  return setmetatable({db = db, const = sqlite.const}, {__index = base_db})
  -- return setmetatable(db, {__index = base_db})
end

return setup
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$ fdm=indent:

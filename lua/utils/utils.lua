local empty_config = {}

-- Not support self-reference
local function loadConfig(default, specific)
  local config = {}
  for k, v in pairs(default) do
    -- if depth then print(depth, ": ", k, v) end
    if k == 'params' then print("detected") end
    if type(v) == 'table' then
      config[k] = loadConfig(v, specific[k] or empty_config)
    else
      config[k] = specific[k] or v
    end
  end
  for k, v in pairs(specific) do
    if not config[k] then
      if type(v) == 'type' then
        config[k] = loadConfig(v)
      else
        config[k] = v
      end
    end
  end
  return config
end

-- UTF-8 编码参考：https://www.cnblogs.com/tsingke/p/10853936.html
local function readUTF8(content, genNext, reset)
  if reset then reset(content) end
  local next = genNext(content)
  local byte = next()

  return function()
    local result = ''

    if not byte:byte() then
      return nil
    elseif byte:byte() >= 0xc0 then
      repeat
        result = result .. byte
        byte = next()
      until not byte:byte() or byte:byte() >= 0xc0 or byte:byte() < 0x80
    elseif byte:byte() >= 0x80 then
      assert(false, "Unexpected byte in readChar")
    elseif byte:byte() < 0x80 then
      result = byte
      byte = next()
    end
    return result
  end
end

local function nextCode4File(f)
  return function()
    return f:read(1)
  end
end
local function resetFile(f)
  f:seek('set')
end

local function nextCode4Str(str)
  local index = 0
  return function()
    index = index + 1
    return str:sub(index, index)
  end
end

-- local fin = io.open('h.txt', 'r')
-- assert(fin, "File open failed.")
-- 
-- local i = 0
-- for c in readUTF8(fin, nextCode4File, resetFile) do
--   print(c)
--   i = i + 1
--   if i > 30 then break end
-- end
-- for c in readUTF8('一个测试', nextCode4Str) do
--   print(c)
-- end
-- 
-- fin:close()

local M = {
  loadConfig = loadConfig,
  readUTF8 = readUTF8,
}

return M

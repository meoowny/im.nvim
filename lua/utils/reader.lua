--
-- table handling
--

local function tset(t, id, value)
  local len = #id
  for i = 1, len do
    local ch = id:sub(i, i)
    if t[ch] == nil then
      t[ch] = {}
    end
    t = t[ch]
  end
  table.insert(t, value)
end

local function tget(t, id)
  local len = #id
  for i = 1, len do
    local ch = id:sub(i, i)
    t = t[ch]
    if t == nil then
      break
    end
  end
  return t
end

-- TODO: const field compatibility with sqlite version
-- TODO: use special data structure to improve the efficiency, such as
--       LSM(maybe it's the name?)

local base_dict = {}

function base_dict:open(name)
  -- To be compatible with sqlite version
  return name
end

local handleCandidate_default_opts = {
  limit = 25,
  offset = 0,
  fuzzy = true,
}
function base_dict:handleCandidate(im, str, func, opts)
  local limit = opts and opts.limit or handleCandidate_default_opts.limit
  local offset = opts and opts.offset or handleCandidate_default_opts.offset
  local fuzzy = opts and opts.fuzzy or handleCandidate_default_opts.fuzzy
  -- im is the name of input method, which to decide which lookup table
  -- will be used.
  local match_table = tget(self.db, str)
  if match_table == nil then
    print("Query failed.")
    do return nil end
  end

  for _, v in ipairs(match_table) do
    func(str, v)
  end
end

function base_dict:close()
  -- To be compatible with sqlite version
  return nil
end

local function DbIterator(t, id, func)
  for k, v in pairs(t) do
    if type(v) == "table" then
      DbIterator(t[k], id .. k, func)
    elseif type(k) == "number" then
      func(id, v)
    end
  end
end

---
--- More Function
---

function base_dict:convert(im_name, outfile)
  local fout = io.open(outfile, "w")
  assert(fout, "fout open failed.")

  -- out of order, due to the pairs
  DbIterator(self.db, "", function(k, v)
    fout:write(k .. ' ' .. v .. '\n')
  end)

  fout:close()
end

function base_dict:remove(im, key, id)
end

function base_dict:insert(im, key, char, id)
end

function base_dict:exchange(im, key, id1, id2)
end

function base_dict:getFullKey(im, char)
end

---
--- Module Setup
---

local function setup(_, dict_path)
  assert(dict_path)
  local dict = {}
  local fin = io.open(dict_path, "r")
  assert(fin, "Lookup table open failed.")

  for line in fin:lines() do
    -- table.insert(dict, line)
    -- new method
    local tmp = vim.fn.split(line, ' ')
    tset(dict, tmp[1], tmp[2])
  end

  fin:close()
  return setmetatable({db = dict}, {__index = base_dict})
end

return setup
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$ fdm=indent:

local dict = {}
local fin = io.open('full.yustar.txt', 'r')
assert(fin,  "Lookup table open failed.")

for line in fin:lines() do
  table.insert(dict, line)
end

fin:close()

local function getFullKey(value)
  local match_id = vim.fn.match(dict, '\\V' .. value) + 1
  if match_id == 0 then
    print("Query failed.")
    do return nil end
  end
  return vim.split(dict[match_id], ' ')[2]
end

local function splitWord(word)
  local result = vim.fn.split(word, '\\zs')
  return result, #result
end

local function combinateKey(word)
  local chars, len = splitWord(word)
  local keys = {}
  local result = ''
  assert(len > 0, 'Unexpected word split result')

  for i, ch in ipairs(chars) do
    table.insert(keys, getFullKey(ch))
    if not keys[i] then
      return nil
    end
  end

  if len == 1 then
    result = keys[1]
  elseif len == 2 then
    result = keys[1]:sub(1, 2) .. keys[2]:sub(1, 2)
  elseif len == 3 then
    result = keys[1]:sub(1, 1) .. keys[2]:sub(1, 1) .. keys[3]:sub(1, 2)
  else
    result = keys[1]:sub(1, 1) .. keys[2]:sub(1, 1) .. keys[3]:sub(1, 1) .. keys[len]:sub(1, 1)
  end

  return result
end

for _, v in ipairs(arg) do
  print(combinateKey(v))
end

-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$ fdm=indent:

local args = {...}
local script_path = vim.api.nvim_get_runtime_file("*/" .. args[1] .. ".lua", false)[1] or ''
-- local script_path = vim.fn.expand("<sfile>:p:h") .. "/"
local tmp = vim.split(vim.fs.normalize(script_path), "/")
script_path = ""
for i = 1, #tmp - 1 do
  script_path = script_path .. tmp[i] .. "/"
end

local default_im_name = "yuhao"
-- local char_map_path = vim.api.nvim_get_runtime_file("*/" .. default_im_name .. ".txt", true)[1] or ''
local char_map_path = script_path .. default_im_name .. ".txt"
-- local db_path = vim.api.nvim_get_runtime_file("*/codes.db", true)[1] or ''
local db_path = script_path .. "codes.db"
local sqlite_path = 'sqlite3'

-- What needs to be config?
-- db_path sqlite_path im_name mb_path
return {
  script_path = script_path,
  db_path = db_path,
  sqlite_path = sqlite_path,
  im_name = default_im_name,  -- table name
  char_map_path = char_map_path,

  -- valid_char = vim.split('abcdefghijklmnopqrstuvwxyz', ''),
  -- select_char = {1, 2, 3, 4, 5, 6, 7, 8, 9},
  punc = { -- punc should include as much as keys
    [' '] = ' ', ['\t'] = '\t', ['`'] = '`', ['~'] = '·',
    ['.'] = '。', [','] = '，', ['/'] = '/', [';'] = '；', ['\''] = {'‘', '’'},
    ['<'] = '《', ['>'] = '》', ['?'] = '？', [':'] = '：', ['\"'] = {'“', '”'},
    ['['] = '【', [']'] = '】', ['\\'] = '、', ['-'] = '-', ['='] = '=',
    ['{'] = '〖', ['}'] = '〗', ['|'] = '|', ['_'] = '——', ['+'] = '+',
    ['!'] = '！', ['@'] = '@', ['#'] = '#', ['$'] = '￥', ['%'] = '%',
    ['^'] = '……', ['&'] = '&', ['*'] = '*', ['('] = '（', [')'] = '）',
  },
  -- Plan to use fsm['punc'] to invoke, instead of deal with them individually
  -- func_key should be subset of punc now
  -- TODO: check func_key
  -- func_key = {
  --   [' '] = {'select', 1}, [';'] = {'select', 2}, ['\''] = {'select', 3}, ['\t'] = {'select', 4},
  --   ['['] = {'page', false}, [']'] = {'page', true},
  -- },
  -- TODO: For switching modes in empty mode
  -- mode_key = {
  -- },
  -- im_config: max_length = 4, valid_char, select_char
}
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$:

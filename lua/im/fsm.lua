local utils = require('utils/utils')
local empty_mode = {}
local chinese_mode = {}
local tmp_en_mode = {}

---
--- General Function
---

-- TODO: characters filter
-- TODO: replace loadConfig with vim.tbl_deep_extend

-- For output the original input chars
local function normal(mode, env, char, output)
  output.output = nil
  output.confirm = true
  env:reset()
  return mode
end

-- Return a callback function of the db:handleCandidate
local function updateCandidate(env, output)
  local index = 0
  return function(code, char)
    -- assert(code, env.db.const.OK)
    -- code usage: print(code)
    index = index + 1
    env.candidate[index] = char
    env.candidate_size = index

    local displayId = index - env.page_offset
    if displayId <= env.page_size and displayId > 0 then
      output.display[displayId] = char
    end
  end
end

---
--- Empty Mode
---

empty_mode.config = {
  ['0'] = {'number'}, ['1'] = {'number'}, ['2'] = {'number'}, ['3'] = {'number'}, ['4'] = {'number'},
  ['5'] = {'number'}, ['6'] = {'number'}, ['7'] = {'number'}, ['8'] = {'number'}, ['9'] = {'number'},
  a = {'letter'}, b = {'letter'}, c = {'letter'}, d = {'letter'}, e = {'letter'}, f = {'letter'}, g = {'letter'},
  h = {'letter'}, i = {'letter'}, j = {'letter'}, k = {'letter'}, l = {'letter'}, m = {'letter'}, n = {'letter'},
  o = {'letter'}, p = {'letter'}, q = {'letter'}, r = {'letter'}, s = {'letter'}, t = {'letter'},
  u = {'letter'}, v = {'letter'}, w = {'letter'}, x = {'letter'}, y = {'letter'}, z = {'letter'},
  [','] = {'punc'}, ['.'] = {'punc'}, ['/'] = {'toTmpEn'}, [';'] = {'punc'}, ['\''] = {'punc'},
  ['<'] = {'punc'}, ['>'] = {'punc'}, ['?'] = {'punc'}, [':'] = {'punc'}, ['\"'] = {'punc'},
  ['['] = {'punc'}, [']'] = {'punc'}, ['\\'] = {'punc'}, ['-'] = {'punc'}, ['='] = {'punc'},
  ['{'] = {'punc'}, ['}'] = {'punc'}, ['|'] = {'punc'}, ['_'] = {'punc'}, ['+'] = {'punc'},
  ['!'] = {'punc'}, ['@'] = {'punc'}, ['#'] = {'punc'}, ['$'] = {'punc'}, ['%'] = {'punc'},
  ['^'] = {'punc'}, ['&'] = {'punc'}, ['*'] = {'punc'}, ['('] = {'punc'}, [')'] = {'punc'},
  ['`'] = {'punc'}, ['~'] = {'punc'},
}

function empty_mode:letter(env, char, output)
  env.number_prefix = false
  if #env.input >= 4 then
    self:select(env, char, output)
  end

  env.page_offset = 0
  env.input = env.input .. char

  output.input = env.input
  env.db:handleCandidate(env.config.im_name, env.input, updateCandidate(env, output))
  return chinese_mode
end

function empty_mode:number(env, char, output)
  env.number_prefix = true
  output.confirm = true
  output.output = nil
  return empty_mode
end

function empty_mode:punc(env, char, output)
  local punc = env.config.punc
  output.confirm = true

  -- TODO: add an origin-output puncs table
  if env.number_prefix and (char == '.' or char == ':') then
    env.number_prefix = false
    output.output = nil
    do return empty_mode end
  end

  env.number_prefix = false
  if type(punc[char]) == 'table' then
    local state = env.punc_state[char] or 1
    env.punc_state[char] = state % #punc[char] + 1
    output.output = output.output .. punc[char][state]
  else
    output.output = output.output .. punc[char]
  end
  return empty_mode
end

function empty_mode:toTmpEn(env, char, output)
  output.confirm = true
  env.number_prefix = false
  return tmp_en_mode
end

---
--- Chinese Mode
---

chinese_mode.config = utils.loadConfig(empty_mode.config, {
  ['1'] = {'select', params = {id = 1}}, ['2'] = {'select', params = {id = 2}},
  ['3'] = {'select', params = {id = 3}}, ['4'] = {'select', params = {id = 4}},
  ['5'] = {'select', params = {id = 5}}, ['6'] = {'select', params = {id = 6}},
  ['7'] = {'select', params = {id = 7}}, ['8'] = {'select', params = {id = 8}},
  ['9'] = {'select', params = {id = 9}},
  [' '] = {'select', params = {id = 1}}, [';'] = {'select', params = {id = 2}},
  ['\''] = {'select', params = {id = 3}}, ['\t'] = {'select', params = {id = 4}},
  ['['] = {'page', params = {isUp = false}}, [']'] = {'page', params = {isUp = true}},
  -- [','] = {'page', params = {isUp = false}}, ['.'] = {'page', params = {isUp = true}},
  ['\n'] = {'enter'}, ['\027'] = {'clear'},
  ['\b'] = {'backspace'}, ['/'] = {'punc'},
  -- ['+'] = {'add'}, ['-'] = {'delete'},
  A = {'caps'}, B = {'caps'}, C = {'caps'}, D = {'caps'}, E = {'caps'}, F = {'caps'}, G = {'caps'},
  H = {'caps'}, I = {'caps'}, J = {'caps'}, K = {'caps'}, L = {'caps'}, M = {'caps'}, N = {'caps'},
  O = {'caps'}, P = {'caps'}, Q = {'caps'}, R = {'caps'}, S = {'caps'}, T = {'caps'},
  U = {'caps'}, V = {'caps'}, W = {'caps'}, X = {'caps'}, Y = {'caps'}, Z = {'caps'},
})

chinese_mode.letter = empty_mode.letter

function chinese_mode:caps(env, char, output)
  self:select(env, char, output)
  output.output = output.output .. ' ' .. char
  return empty_mode
end

function chinese_mode:punc(env, char, output)
  self:select(env, char, output)
  empty_mode:punc(env, char, output)
  return empty_mode
end

function chinese_mode:select(env, char, output, params)
  local index = env.page_offset + (params and params.id or 1)
  local selected_char = index <= env.candidate_size and env.candidate[index] or ''
  env:reset()
  output:reset()

  output.confirm = true
  output.output = selected_char
  return empty_mode
end

function chinese_mode:clear(env, char, output)
  output.confirm = true
  env:reset()
  return empty_mode
end

function chinese_mode:backspace(env, char, output)
  local len = #env.input
  if len ~= 0 then
    env.input = env.input:sub(1, -2)
  end

  if len == 1 then
    env:reset()
    output.confirm = true
    output.output = ''
    return empty_mode
  else
    env.db:handleCandidate(env.config.im_name, env.input, updateCandidate(env, output))
    output.input = env.input
    return chinese_mode
  end
end

function chinese_mode:enter(env, char, output)
  output.output = env.input
  output.confirm = true
  env:reset()
  return empty_mode
end

function chinese_mode:page(env, char, output, params)
  if params and params.isUp and env.candidate_size >= (env.page_offset + env.page_size + 1) or not params then
    env.page_offset = env.page_offset + env.page_size
  elseif params and not params.isUp and env.page_offset > 0 then
    env.page_offset = env.page_offset - env.page_size
  end

  local offset = env.page_offset
  output.input = env.input
  for i = 1, env.page_size do
    output.display[i] = (offset + i) <= env.candidate_size and env.candidate[offset + i] or nil
  end
  return chinese_mode
end

---
--- Temp Engligh Mode (Or Command Mode?)
---

tmp_en_mode.config = {
  ['/'] = {'punc'}, [' '] = {'punc'}, ['\n'] = {'toEmpty'},
  [':'] = {'toCmd'}, ['\027'] = {'toEmpty'}
}

tmp_en_mode.toCmd = normal

function tmp_en_mode:toEmpty(env, char, output)
  output.confirm = true
  return empty_mode
end

function tmp_en_mode:punc(env, char, output)
  empty_mode:punc(env, char, output)
  return empty_mode
end

---
--- Interface
---

local function M(db, user_config)
  local fsm = empty_mode
  local env = {
    db = db,  -- db = { db = db(cdata), base_db functions}
    -- TODO: buffer and offset
    config = user_config,
    number_prefix = false,
    punc_state = {},

    input = '',
    page_offset = 0,
    page_size = 5,
    candidate_size = 0,
    candidate = {},

    reset = function(self)
      self.number_prefix = false
      self.input = ''
      self.page_offset = 0
      self.candidate_size = 0
      for k in pairs(self.candidate) do self.candidate[k] = nil end
      -- FIX: Wait to be discussed
      -- for k, _ in pairs(self.punc_state) do
      --   self.punc_state[k] = 1
      -- end
    end,
  }
  -- @param output: nil for origin input and '' for empty output, when
  --                confirm is true the output is written on the screen.
  -- @param confirm: whether the output should be written.
  -- @param page_offset: work with display and candidate, which range of
  --                    candidate items will be displayed.
  --                    (moved to the env)
  -- @param candidate: all output items.
  --                   (moved to the env)
  -- @param display: items to be displayed, include items to be selected
  --                 and input chars.
  -- @param input: what codes user input. See usage below.
  --
  -- confirm = true and output = nil means the origin input will be written
  -- confirm = true and input = nil means the IMInserted should be
  --   triggered.
  local output = {
    input = nil,
    output = '',
    confirm = false,
    display = {},  -- include input item to display
    reset = function(self)
      self.input = nil
      self.output = ''
      self.confirm = false
      for k in pairs(self.display) do self.display[k] = nil end
    end,
  }

  return function(char)
    output:reset()
    local func = fsm.config[char]
    if func then
      fsm = fsm[func[1]](fsm, env, char, output, func.params)
    else
      normal(fsm, env, char, output)
    end
    return output
  end
end

return M
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$ fdm=indent:

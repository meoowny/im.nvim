-- Usage:
--   1. run :ConvertOne in this directory first to generate chars.txt
--   2. cat setup.sql | sqlite3 codes.db
CREATE TABLE IF NOT EXISTS yuhao(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    key VARCHAR(4) NOT NULL,
    value NVARCHAR(8) NOT NULL,
    weight INTEGER DEFAULT 0
);

-- 导入码表
CREATE TABLE tempTable(
    key VARCHAR(4) NOT NULL,
    value NVARCHAR(8) NOT NULL,
    weight INTEGER
);

.separator " "
.import chars.txt tempTable

INSERT INTO yuhao(key, value, weight)
SELECT * FROM tempTable;

DROP TABLE tempTable;

-- 创建索引
CREATE INDEX code ON yuhao(key, weight);

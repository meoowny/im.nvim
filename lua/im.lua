local utils = require('utils/utils')
local im_generator = require("im.fsm")
local config, db
local M = {}

local convert = {
  ['<ESC>'] = '\027', ['<CR>'] = '\n', ['<SPACE>'] = ' ', ['<TAB>'] = '\t', ['<BS>'] = '\b',
}

local popup_bar = {}
local popup_opts = {
  id = 1,
  virt_lines = {popup_bar},
  virt_lines_above = false,
  virt_text_pos = "overlay",
}

-- handle the output table, including displaying candidate bar
local function handler(im, char)
  local ch = convert[char] or char
  return function()
    local output = im(ch)

    for k, _ in pairs(popup_bar) do popup_bar[k] = nil end
    local i = 1
    popup_bar[i] = {string.format("%-4s", output.input or ''), "PmenuSel"}
    for _, v in ipairs(output.display) do
      i = i + 1
      popup_bar[i] = {' ' .. tostring(i - 1) .. ' ' .. v, "Pmenu"}
    end

    -- Get cursor position
    local current_row = vim.api.nvim_win_get_cursor(0)[1] - 1
    -- popup_opts.virt_lines_above = current_row ~= 0
    vim.api.nvim_buf_set_extmark(0, vim.b.ns_id, current_row, 0, popup_opts)

    if output.confirm then
      if not output.input then
        vim.cmd [[doautocmd User IMInserted]]
      else
        vim.cmd [[echohl Pmenu | echon "hi"]]
      end
      return output.output or char
    end
  end
end

---
--- Plugin Setup(TODO: lazy load)
---

local function bufSetup(user_config)
  vim.b.ns_id = vim.api.nvim_create_namespace("im" .. tonumber(vim.api.nvim_get_current_buf()))
  vim.b.keymap_name = user_config.im_name
  vim.b.im = im_generator(db, user_config)
end

function M.setup(user_config)
  -- config = utils.loadConfig(require('config'), user_config)
  config = vim.tbl_deep_extend('force', require('im.config'), user_config)
  if not user_config.use_table_engine then
    db = require('utils/loader')(config.sqlite_path, config.db_path)
  elseif (user_config.engine or '') == 'matcher' then
    db = require('utils/matcher')(config.sqlite_path, config.db_path)
  else
    -- use regex engine(slowly load, about 1s):
    db = require('utils/reader')(config.sqlite_path, config.db_path)
  end
  bufSetup(config)

  -- keymap set: all puncs and letters
  for i = 33, 126 do
    vim.keymap.set('l', string.char(i), handler(vim.b.im, string.char(i)),
      { expr = true, noremap = true, buffer = false, silent = true, desc = "Input Method Keymap" })
  end

  for k, _ in pairs(convert) do
    vim.keymap.set('l', k, handler(vim.b.im, k),
      { expr = true, noremap = true, buffer = false, silent = true })
  end
  vim.keymap.set({'i', 'c'}, '<C-S>', '<C-^>',
    { noremap = true, buffer = false, silent = true })
  vim.keymap.set('n', '<C-S>', 'a<C-^><ESC>',
    { noremap = true, buffer = false, silent = true })
end

---
--- AutoCommand
---

vim.api.nvim_create_autocmd({'User'}, {
  pattern = 'IMInserted',
  callback = function()
    vim.api.nvim_buf_clear_namespace(0, vim.b.ns_id, 0, -1)
  end,
})
vim.api.nvim_create_autocmd({'BufEnter'}, {
  pattern = "*",
  callback = function()
    bufSetup(config)
  end,
})
vim.api.nvim_create_autocmd({'VimLeave'}, {
  pattern = '*',
  callback = function() db:close() end,
})

---
--- User Commands
---

local function convertMap(dst, src)
  local fin = io.open(src)
  local fout = io.open(dst, "w")
  assert(fin and fout, "File open failed.")

  local freq = 100
  local old_code = ""
  for line in fin:lines() do
    local new_code = line:match("%a+")
    if old_code ~= new_code then
      old_code = new_code
      freq = 100
    else
      freq = freq - 1
    end

    fout:write(line .. " " .. freq .. "\n")
  end

  fin:close()
  fout:close()
end

-- Out of order
local function setupReadFile(file_name)
  local t = {}
  local file = io.open(file_name)
  assert(file, "File open failed.")

  for line in file:lines() do
    local key = line:match("%a+")
    if not t[key] then
      t[key] = {}
    end
    table.insert(t[key], line)
  end

  file:close()
  return t
end

vim.api.nvim_create_user_command("GetImPath", function()
  print(config.script_path)
end, {nargs = 0})
vim.api.nvim_create_user_command("ConvertOne", function()
  convertMap("yuhao_" .. os.date("%y%m%d_%H%M") .. ".txt", "yuhao.txt")
  convertMap("chars.txt", "yuhao.txt")
end, {nargs = 0})
vim.api.nvim_create_user_command("ConvertTwo", function()
  local test = io.open("test.txt", "w")
  assert(test, "File open failed.")
  for _, line in pairs(setupReadFile("yuhao.txt")) do
    for k, v in pairs(line) do
      test:write(v, " ", 100 - k, "\n")
    end
  end
end, {nargs = 0})

-- Database To Text(Experimental function)
vim.api.nvim_create_user_command("ConvertDb", function(opts)
  assert(db, "db not found.")
  assert(db.convert, "db convert function not found.")
  db:convert(config.im_name, "db.txt", opts.fargs[1] == '1')
end, {
    nargs = 1,
    complete = function (ArgLead, CmdLine, CursorPos)
      return {"1", "0"}
    end
  })

---
--- Characters Map Update
---

local function getPrompt(prompt, match_chars)
  return {
    prompt = prompt,
    format_item = function(item)
      return match_chars[item]
    end,
  }
end
local function confirmOperation(prompt, func)
  vim.ui.select({'yes', 'no'},
    { prompt = prompt },
    function (choice)
      if choice == 'yes' then
        func()
      end
    end)
end

vim.api.nvim_create_user_command("TestForDelete", function(opts)
  local key = opts.fargs[1]
  local match_chars = {}
  db:handleCandidate('yuhao', key, function(code, char)
    table.insert(match_chars, char)
  end, {fuzzy = false})

  vim.ui.select(vim.fn.range(1, #match_chars),
    getPrompt('Delete this characters?', match_chars),
    function(id)
      if type(id) ~= 'number' then
        do return nil end
      end
      confirmOperation('\nConfirm?', function()
        db:remove("yuhao", key, id)
      end)
    end)
end, {nargs = 1})
vim.api.nvim_create_user_command("TestForExchange", function(opts)
  local key = opts.fargs[1]
  local match_chars = {}
  db:handleCandidate('yuhao', key, function(code, char)
    table.insert(match_chars, char)
  end, {fuzzy = false})

  vim.ui.select(vim.fn.range(1, #match_chars),
    getPrompt('Exchange which characters?', match_chars),
    function (id1)
      if type(id1) ~= 'number' then
        do return nil end
      end
      vim.ui.select(vim.fn.range(1, #match_chars),
        getPrompt('\nWith which characters?', match_chars),
        function(id2)
          if type(id2) ~= 'number' or id1 == id2 then
            vim.notify("\nExchange failed.", vim.log.levels.INFO)
            do return nil end
          end
          confirmOperation('\nConfirm to exchange them?', function()
            db:exchange('yuhao', key, id1, id2)
          end)
        end)
    end)
end, {nargs = 1})
-- TODO:
vim.api.nvim_create_user_command("TestForInsert", function()
  db:insert("yuhao", "a", "打", 2)
end, {nargs = 0})
vim.api.nvim_create_user_command("GetFullKey", function(opts)
  vim.notify(db:getFullKey('yuhao', opts.fargs[1]), vim.log.levels.INFO)
end, {nargs = 1})

return M
-- vim: set tw=75 ts=2 sw=2 nu nuw=4 list lcs=eol\:$:

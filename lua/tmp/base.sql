CREATE TABLE IF NOT EXISTS yuhao(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    key TEXT NOT NULL,
    value TEXT NOT NULL,
    weight INTEGER DEFAULT 0
);

-- 导入码表
CREATE TABLE tempTable(
    key TEXT NOT NULL,
    value TEXT NOT NULL,
    weight INTEGER
);

.separator " "
.import ../../vim-glyph/plugin/yuhao_origin.txt tempTable

INSERT INTO yuhao(key, value, weight)
SELECT * FROM tempTable;

DROP TABLE tempTable;

-- 创建索引
CREATE INDEX code ON yuhao(key, weight);

-- 查询编码
SELECT *
FROM yuhao
WHERE key LIKE 'aa_'
ORDER BY weight DESC, key ASC
LIMIT 5;

-- 新增编码
INSERT INTO yuhao(key, value, weight)
VALUES ('aaje', '𣀋', 100);

UPDATE yuhao
SET weight = weight - 1
WHERE key = 'aaje';
INSERT INTO yuhao(key, value, weight)
VALUES ('aaje', '是', (
        SELECT MAX(weight) + 1 FROM yuhao
        WHERE key = 'aaje'
));

-- 删除编码
DELETE FROM yuhao
WHERE id = 2 AND key = 'aaje';

DELETE FROM yuhao
WHERE weight = 101 - 2 AND key = 'aaje';

-- 调整词频
UPDATE yuhao
SET weight = 99
WHERE id = 3 AND key = 'aaje';

